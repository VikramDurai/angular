import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Register } from '../student-login/register/register';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  userData: Register[];

  constructor(private formBuilder: FormBuilder,
    private homeService: HomeService) {
  }

  ngOnInit(): void {
    this.homeService.getAllStudent().subscribe(res => {
      this.userData = res;
    })
  }
}