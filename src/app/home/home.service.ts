import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../shared.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private sharedService: SharedService, private httpClient: HttpClient) { }
  getAllStudent(): Observable<any> {
    return this.httpClient.get(this.sharedService.baseUrl + '/getallstudent');
  }
}
