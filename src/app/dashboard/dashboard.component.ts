import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private cookieService: CookieService,
    private router: Router) {
    if (!this.cookieService.get('isLogOut')) {
      this.router.navigate(['']);
    }
  }

  ngOnInit(): void {
  }

  logoutClicked(): void {
    this.cookieService.delete('isLogOut');
  }
}
