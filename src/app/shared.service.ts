import {Injectable} from '@angular/core';
import {Login} from './student-login/login/login';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Register } from './student-login/register/register';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  loginModel: Login;
  registerModel: Register;
  public baseUrl = 'http://localhost:3000';
  public headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line:typedef
  getLoginData() {
    return this.loginModel;
  }

  getRegisteredData() {
    console.log('test is the--->' + this.registerModel);

    return this.registerModel;
    
  }

  // createStudent(data: any): Observable<any> {
  //   console.log('testing  ', data);
  //   return this.http.post<any>(this.baseUrl + '/createstudent', data);
  // }
}


