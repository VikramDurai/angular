/* tslint:disable */
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from '../../shared.service';
import { LoginService } from './login.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class StudentLoginComponent implements OnInit {
  loginForm: FormGroup;
  disableBtn : Boolean;

  constructor(private route: Router,
    private sharedService: SharedService,
    private loginService: LoginService,
    private cookieService: CookieService
  ) {
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      name: new FormControl('')
    });
    this.loginForm.valueChanges
      .subscribe((changedObj: any) => {
        this.disableBtn = this.loginForm.valid;
      });
  }

  async studentLogin(): Promise<void> {
    var userExist = false;
    if (this.loginForm.invalid) {
      return
    }
    console.log('this.login', this.loginForm.value.username, ' password', this.loginForm.value.password);
    if (this.loginForm.value.username !== '' && this.loginForm.value.password !== '') {
      this.sharedService.loginModel = this.loginForm.value;
      this.loginService.getAllStudent().subscribe(res => {
        res.forEach(data => {
          if ((this.loginForm.value.username === data.studentEmail) && (this.loginForm.value.password === data.studentPassword)) {
            userExist = true;
          }
        });
        if (userExist) {
          this.cookieService.set('isLogOut', 'true');
          this.route.navigate(['/home']);
        } else {
          alert('user does not exist');
          this.loginForm.reset();
        }
      });
    }
  }
  get f() { return this.loginForm.controls; }
}
