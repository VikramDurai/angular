import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from './login';
import { Observable } from 'rxjs';
import { SharedService } from '../../shared.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  loginModel: Login;
  constructor(private http: HttpClient,
    private sharedService: SharedService) { }


  getAllStudent(): Observable<any> {
    return this.http.get(this.sharedService.baseUrl + '/getallstudent');
  }
}
