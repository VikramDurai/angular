import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from './register.service';
import { SharedService } from '../../shared.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class StudentRegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false
  disableBtn: boolean;

  constructor(private registerService: RegisterService,
    private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      studentName: new FormControl('', Validators.required),
      studentEmail: new FormControl('', Validators.required),
      studentPassword: new FormControl('', Validators.required),
      studentPhone: new FormControl('')
    });
    this.registerForm.valueChanges
      .subscribe((changedObj: any) => {
        this.disableBtn = this.registerForm.valid;
      });
  }

  async studentRegister(): Promise<void> {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return
    }
    this.sharedService.registerModel = this.registerForm.value;
    console.log('shared service-->', this.sharedService.registerModel);
    await this.registerService.postStudent(this.registerForm.value).subscribe(res => {
      console.log('res, ', res);
    });
    this.registerForm.reset();
  }
  get f() { return this.registerForm.controls; }
}
