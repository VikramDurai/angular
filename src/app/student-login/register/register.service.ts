import {Injectable} from '@angular/core';
import {Register} from './register';
import {HttpClient} from '@angular/common/http';
import {SharedService} from '../../shared.service';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  registerForm = Register;

  constructor(private http: HttpClient,
              private sharedService: SharedService) {
  }

  postStudent(postData: any): Observable<any> {
    return this.http.post(this.sharedService.baseUrl + '/createstudent', postData);
  }
}
