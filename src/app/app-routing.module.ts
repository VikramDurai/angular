import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentLoginComponent} from './student-login/login/login.component';
import {StudentRegisterComponent} from './student-login/register/register.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/student-login',
    pathMatch: 'full'
  },
  {
    path: 'student-login',
    component: StudentLoginComponent
  }, {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'student-register',
    component: StudentRegisterComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
